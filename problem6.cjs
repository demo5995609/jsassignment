let invent = require('./car.cjs');
let inventory = invent.inventory;

let result = bmwAudiCars(inventory);
console.log(JSON.stringify(result));

//Function created  for list of BMW and Audi
function bmwAudiCars(inventory) {
    if(inventory == null || !Array.isArray(inventory)){
        return [];
    }
    let arr = [];
    for(let idx = 0; idx<inventory.length; idx++){
        let make = inventory[idx].car_make;
        if(make == 'BMW' || make == 'Audi'){
            arr.push(inventory[idx]);
        }
    }
    return arr;
}
