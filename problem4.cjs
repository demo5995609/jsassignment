let invent = require('./car.cjs');
let inventory = invent.inventory;

let result = getYears(inventory);

console.log(result);

//Creating getYear in array form
function getYears(inventory) {
    if(inventory == null || !Array.isArray(inventory)){
        return [];
    }
    let arr = [];
    for(let idx = 0; idx<inventory.length; idx++){
        arr.push(inventory[idx].car_year);
    }
    return arr;
}

// console.log(arr);