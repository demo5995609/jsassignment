let problem1 = require('./problem1.cjs');
let cars = require('./car.cjs');
let id = 33;
let result = problem1(cars.inventory, id);

// let result = findById33([{"id":33,"car_make":"Dodge","car_model":"Magnum","car_year":2008}], 33)[0];
// console.log(result);
if(result.length<1){
  console.log([]);
}else{
  let str = 'Car 33 is a '+result[0].car_year+' '+result[0].car_make+' '+result[0].car_model;
  console.log(str);
}
