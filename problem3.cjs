let invent = require('./car.cjs');
let inventory = invent.inventory;

sortByModels(inventory);

//sorting with arrow functions
function sortByModels(inventory) {
    if(inventory == null || !Array.isArray(inventory)){
        return [];
    }
    inventory.sort((a, b) =>{
        // console.log(JSON.stringify(a.car_model).toLowerCase());
        if(a.car_model.toLowerCase()<b.car_model.toLowerCase()){
            return -1;
        }else{
            return 1;
        }
        
    })

console.log(inventory);
}
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
