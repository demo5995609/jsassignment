const invent = require('./car.cjs');
const inventory = invent.inventory;
let id = 33;

let result = findById33(inventory, id);
// console.log(result);
if(result.length==0){
  console.log([]);
}else{
  let str = 'Car 33 is a '+result.car_year+' '+result.car_make+' '+result.car_model;
  console.log(str);
}


function findById33(inventory, id){

  // let arr = [];
  if(id == undefined || inventory == undefined || typeof id != 'number' || !Array.isArray(inventory)){
    return [];
  }

  for(let idx = 0; idx<inventory.length; idx++){
    if(inventory[idx].id == id){
      return inventory[idx];
    }
  }
  return [];
}
module.exports = findById33;